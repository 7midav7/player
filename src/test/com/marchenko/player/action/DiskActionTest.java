package test.com.marchenko.player.action;

import com.marchenko.player.action.DiskAction;
import com.marchenko.player.entity.composition.*;
import com.marchenko.player.entity.disk.Disk;
import com.marchenko.player.entity.disk.TypeDisk;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

/**
 * Created by Marchenko Vadim on 9/26/2015.
 */
@RunWith(Parameterized.class)
public class DiskActionTest {
    private ArrayList<Composition> compositions;

    public DiskActionTest(ArrayList<Composition> compositions) {
        this.compositions = compositions;
    }

    @Parameterized.Parameters
    public static Collection<Object> valuesTesting(){
        ArrayList<Composition> list = new ArrayList<>();
        ArrayList tests = new ArrayList<>();

        list.add(new Blues(12, 234, "Slave", Blues.Region.CHICAGO));
        list.add(new Classical(2, 40, "RV111 Presto", Classical.Period.CLASSICAL, "Vienna orchestra"));
        list.add(new Classical(8, 974, "Turkish march", Classical.Period.BAROQUE, "Vienna orchestra"));
        list.add(new Classical(11, 9084, "RV495 Andante", Classical.Period.CONTEMPORARY, "Vienna orchestra"));
        list.add(new Electronic(1, 2, "Miracle", 231));
        list.add(new Electronic(1, 1, "Angel", 43));
        list.add(new International(3, 67, "Varen'ka", "Russia"));
        list.add(new Rap(5, 434, "Everyday", Rap.Subgenre.HIP_HOP, "ASAP ROCKY"));
        list.add(new Rock(10, 562, "Something in the Air", Rock.Subgenre.ALTERNATIVE, "Joy Division", " Ian Curtis"));
        list.add(new Rock(7, 872, "Blue Sky", Rock.Subgenre.HEAVY_METAL, "Metallica", "James Hatfield"));
        list.add(new Rock(9, 234, "A Hard Day", Rock.Subgenre.PSYCHEDELIC, "Pink Floyd", "David Jon Gilmour"));
        list.add(new Composition(21, 765, "Undef", "s"));

        for (int i = 0; i < 50; ++ i){
            tests.add(new ArrayList<>(list));
            Collections.shuffle(list);
        }

        return tests;
    };

    @Test
    public void sortGenreTest(){
        DiskAction diskAction = new DiskAction();
        Disk disk = new Disk(1, "Test", TypeDisk.DVD1);
        disk.addAll(compositions);
        diskAction.sortGenre(disk);
        ArrayList<Composition> actualList = new ArrayList<>(disk);

        boolean actual = true;
        Composition previous = null;
        for (Composition comp: actualList){
            if (previous != null && (previous.getGenre().compareTo(comp.getGenre()) > 1)){
                actual = false;
            }
            previous = comp;
        }

        Assert.assertEquals(true, actual);
    }
}
