package test.com.marchenko.player.validator.composition;

import com.marchenko.player.entity.composition.Electronic;
import com.marchenko.player.validator.composition.ElectronicValidator;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

/**
 * Created by Marchenko Vadim on 9/25/2015.
 */
@RunWith(Parameterized.class)
public class ElectronicValidatorTest {
    private int id;
    private int duration;
    private String name;
    private int beatsPerSecond;

    public ElectronicValidatorTest(int id, int duration, String name, int beatsPerSecond) {
        this.id = id;
        this.duration = duration;
        this.name = name;
        this.beatsPerSecond = beatsPerSecond;
    }

    @Parameterized.Parameters
    public static Collection<Object> valuesTesting(){
        return Arrays.asList(new Object[][]{
                {1, 1, "Test song", 78},
                {0, 1, "Test song", 78},
                {-1, 1, "Test song", 78},
                {1, -1, "Test song", 78},
                {1, 0, "Test song", 78},
                {1, 1, null, 78},
                {1, 1, "", 78},
                {1, 1, "Test song", 30},
                {1, 1, "Test song", 29},
                {1, 1, "Test song", 240},
                {1, 1, "Test song", 241},
        });
    }

    @Test
    public void isValidTest(){
        ElectronicValidator electronicValidator = new ElectronicValidator();
        Electronic electronic = new Electronic(id, duration, name, beatsPerSecond);

        boolean actual = electronicValidator.isValid(electronic);
        boolean expected = id > 0;
        expected &= duration > 0;
        expected &= (name!=null && !name.isEmpty());
        expected &= (beatsPerSecond>=30 && beatsPerSecond<=240);

        Assert.assertEquals(expected, actual);
    }
}
