package test.com.marchenko.player.validator.composition;

import com.marchenko.player.entity.composition.Rock;
import com.marchenko.player.validator.composition.RockValidator;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

/**
 * Created by Marchenko Vadim on 9/26/2015.
 */
@RunWith(Parameterized.class)
public class RockValidatorTest {
    private int id;
    private int duration;
    private String name;
    private String vocal;
    private String band;

    public RockValidatorTest(int id, int duration, String name, String vocal, String band) {
        this.id = id;
        this.duration = duration;
        this.name = name;
        this.vocal = vocal;
        this.band = band;
    }

    @Parameterized.Parameters
    public static Collection<Object> valuesTesting(){
        return Arrays.asList( new Object[][]{
                {1, 1, "Test song", "v", "b"},
                {0, 1, "Test song", "v", "b"},
                {-1, 1, "Test song", "v", "b"},
                {1, -1, "Test song", "v", "b"},
                {1, 0, "Test song", "v", "b"},
                {1, 1, null, "v", "b"},
                {1, 1, "", "v", "b"},
                {1, 1, "Test song", "", "b"},
                {1, 1, "Test song", null, "b"},
                {1, 1, "Test song", "v", ""},
                {1, 1, "Test song", "v", null},
        });
    }

    @Test
    public void isValidTest(){
        Rock rock = new Rock(id, duration, name, Rock.Subgenre.ALTERNATIVE, band, vocal);
        boolean expected = id > 0 && duration > 0 && name != null && !name.isEmpty() &&
                band != null && !band.isEmpty() && vocal != null && !vocal.isEmpty();
        boolean actual = new RockValidator().isValid(rock);

        Assert.assertEquals(expected, actual);
    }
}
