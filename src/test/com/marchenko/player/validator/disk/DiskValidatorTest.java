package test.com.marchenko.player.validator.disk;

import com.marchenko.player.entity.disk.Disk;
import com.marchenko.player.entity.disk.TypeDisk;
import com.marchenko.player.validator.disk.DiskValidator;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

/**
 * Created by Marchenko Vadim on 9/26/2015.
 */
@RunWith(Parameterized.class)
public class DiskValidatorTest {
    private int id;
    private String name;

    public DiskValidatorTest(int id, String name) {
        this.id = id;
        this.name = name;
    }

    @Parameterized.Parameters
    public static Collection<Object> valuesTesting(){
        return Arrays.asList(new Object[][]{
                {-1, null},
                {-1, ""},
                {-1, "s"},
                {0, null},
                {0, ""},
                {0, "s"},
                {1, null},
                {1, ""},
                {1, "s"},
        });
    }

    @Test
    public void isValidTest(){
        Disk disk = new Disk(id, name, TypeDisk.DVD3);
        boolean actual = new DiskValidator().isValid(disk);
        boolean expected = ( id > 0 && name != null && !name.isEmpty());

        Assert.assertEquals(expected, actual);
    }
}
