package com.marchenko.player.creator;

import com.marchenko.player.entity.composition.*;
import com.marchenko.player.validator.Validator;
import com.marchenko.player.validator.ValidatorShell;
import com.marchenko.player.validator.composition.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.stream.Collectors;

/**
 * Created by Marchenko Vadim on 9/23/2015.
 */
public class CompositionCreator {
    private static final Logger LOGGER = LogManager.getLogger(CompositionCreator.class);

    public ArrayList<Composition> create(){
        ArrayList<ValidatorShell<? extends Composition>> shells = new ArrayList<>();

        BluesValidator bluesValidator = new BluesValidator();
        ClassicalValidator classicalValidator = new ClassicalValidator();
        ElectronicValidator electronicValidator = new ElectronicValidator();
        InternationalValidator internationalValidator = new InternationalValidator();
        RapValidator rapValidator = new RapValidator();
        RockValidator rockValidator = new RockValidator();
        CompositionValidator compositionValidator = new CompositionValidator();

        Validator<Blues> bluesValidatorImp = (obj) -> (bluesValidator.isValid(obj));
        Validator<Classical> classicalValidatorImp = (obj) -> (classicalValidator.isValid(obj));
        Validator<Electronic> electronicValidatorImp = (obj) -> (electronicValidator.isValid(obj));
        Validator<International> internationalValidatorImp = (obj) -> (internationalValidator.isValid(obj));
        Validator<Rap> rapValidatorImp = (obj) -> (rapValidator.isValid(obj));
        Validator<Rock> rockValidatorImp = (obj) -> (rockValidator.isValid(obj));
        Validator<Composition> compositionValidatorImp = (obj) -> (compositionValidator.isValid(obj));

        shells.add(new ValidatorShell<>(
                new Blues(1, 234, "Slave", Blues.Region.CHICAGO), bluesValidatorImp));
        shells.add(new ValidatorShell<>(
                new Classical(2, 40, "RV111 Presto", Classical.Period.CLASSICAL, "Vienna orchestra"), classicalValidatorImp));
        shells.add(new ValidatorShell<>(
                new Classical(3, 974, "Turkish march", Classical.Period.BAROQUE, "Vienna orchestra"), classicalValidatorImp));
        shells.add(new ValidatorShell<>(
                new Classical(4, 9084, "RV495 Andante", Classical.Period.CONTEMPORARY, "Vienna orchestra"), classicalValidatorImp));
        shells.add(new ValidatorShell<>(
                new Electronic(5, 1, "Miracle", 231), electronicValidatorImp));
        shells.add(new ValidatorShell<>(
                new Electronic(6, 1, "Angel", 43), electronicValidatorImp));
        shells.add(new ValidatorShell<>(
                new International(7, 67, "Varen'ka", "Russia"), internationalValidatorImp));
        shells.add(new ValidatorShell<>(
                new Rap(8, 434, "Everyday", Rap.Subgenre.HIP_HOP, "ASAP ROCKY"), rapValidatorImp));
        shells.add(new ValidatorShell<>(
                new Rock(9, 562, "Something in the Air", Rock.Subgenre.ALTERNATIVE, "Joy Division", " Ian Curtis"), rockValidatorImp));
        shells.add(new ValidatorShell<>(
                new Rock(10, 872, "Blue Sky", Rock.Subgenre.HEAVY_METAL, "Metallica", "James Hatfield"), rockValidatorImp));
        shells.add(new ValidatorShell<>(
                new Rock(11, 234, "A Hard Day", Rock.Subgenre.PSYCHEDELIC, "Pink Floyd", "David Jon Gilmour"), rockValidatorImp));
        shells.add(new ValidatorShell<>(
                new Composition(21, 765, "Undef", "s"), compositionValidatorImp));

        return shells.stream().filter(shell -> {
            if (!shell.isValid()) {
                LOGGER.warn("Invalid composition " + shell.getUncheckedObject());
            }
            return shell.isValid();
        }).map(ValidatorShell::getUncheckedObject).collect(Collectors.toCollection(ArrayList::new));
    }

}
