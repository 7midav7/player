package com.marchenko.player.creator;

import com.marchenko.player.entity.disk.Disk;
import com.marchenko.player.entity.disk.TypeDisk;
import com.marchenko.player.exception.DiscException;
import com.marchenko.player.validator.disk.DiskValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Created by Marchenko Vadim on 9/23/2015.
 */
public class DiskCreator {
    public static final Logger LOGGER = LogManager.getLogger(DiskCreator.class);

    public Disk create() throws DiscException{
        DiskValidator dv = new DiskValidator();
        Disk disk = new Disk(1, "For Afanasiy", TypeDisk.DVD14);
        if (dv.isValid(disk)){
            return disk;
        }
        LOGGER.fatal("Can't create disk.");
        throw new DiscException();
    }
}
