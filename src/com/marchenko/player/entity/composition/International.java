package com.marchenko.player.entity.composition;

/**
 * Created by Marchenko Vadim on 9/24/2015.
 */
public class International extends Composition{
    private String region;

    public International(int id, int duration, String name, String region) {
        super(id, duration, name, "international");
        this.region = region;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

}
