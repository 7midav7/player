package com.marchenko.player.entity.composition;

/**
 * Created by Marchenko Vadim on 9/22/2015.
 */
public class Rap extends Composition {
    public enum Subgenre {HIP_HOP, ALTERNATIVE, POP_RAP, REGGAETON};
    private Subgenre subgenre;
    private String rapper;

    public Rap(int id, int duration, String name, Subgenre subgenre, String rapper) {
        super(id, duration, name, "rap");
        this.subgenre = subgenre;
        this.rapper = rapper;
    }

    public Subgenre getSubgenre() {
        return subgenre;
    }

    public void setSubgenre(Subgenre subgenre) {
        this.subgenre = subgenre;
    }

    public String getRapper() {
        return rapper;
    }

    public void setRapper(String rapper) {
        this.rapper = rapper;
    }

}
