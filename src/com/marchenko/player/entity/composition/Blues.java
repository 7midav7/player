package com.marchenko.player.entity.composition;

/**
 * Created by Marchenko Vadim on 9/22/2015.
 */
public class Blues extends Composition {
    public enum Region{CHICAGO, TEXAS, LOUISIANA, EASTCOAST, WESTCOAST, BRITISH};
    private Region region;

    public Blues(int id, int duration, String name, Region region){
        super(id, duration, name, "blues");
        this.region = region;
    }

    public Region getRegion() {
        return region;
    }

    public void setRegion(Region region) {
        this.region = region;
    }

}
