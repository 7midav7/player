package com.marchenko.player.entity.composition;

/**
 * Created by Marchenko Vadim on 9/22/2015.
 */
public class Classical extends Composition {
    public enum Period{MEDIEVAL, RENAISSANCE, BAROQUE, CLASSICAL, ROMANTIC,
        IMPRESSIONIST, MODERN, CONTEMPORARY};
    private Period period;
    private String orchestraName;

    public Classical(int id, int duration, String name, Period period, String orchestraName) {
        super(id, duration, name, "classical");
        this.period = period;
        this.orchestraName = orchestraName;
    }

    public Period getPeriod() {
        return period;
    }

    public String getOrchestraName() {
        return orchestraName;
    }

    public void setPeriod(Period period) {
        this.period = period;
    }

    public void setOrchestraName(String orchestraName) {
        this.orchestraName = orchestraName;
    }

}
