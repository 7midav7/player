package com.marchenko.player.entity.composition;

/**
 * Created by Marchenko Vadim on 9/22/2015.
 */
public class Rock extends Composition {
    public enum Subgenre{ALTERNATIVE, HEAVY_METAL, ART, HARD, PSYCHEDELIC}
    private Subgenre subgenre;
    private String band;
    private String vocal;

    public Rock(int id, int duration, String name, Subgenre subgenre, String band, String vocal) {
        super(id, duration, name, "rock");
        this.subgenre = subgenre;
        this.band = band;
        this.vocal = vocal;
    }

    public String getBand() {
        return band;
    }

    public void setBand(String band) {
        this.band = band;
    }

    public String getVocal() {
        return vocal;
    }

    public void setVocal(String vocal) {
        this.vocal = vocal;
    }

}
