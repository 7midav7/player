package com.marchenko.player.entity.composition;

/**
 * Created by Marchenko Vadim on 9/22/2015.
 */
public class Composition {
    private int id;
    private String name;
    private int duration;
    private String genre;

    public Composition(int id, int duration, String name, String genre) {
        this.duration = duration;
        this.id = id;
        this.name = name;
        this.genre = genre.toLowerCase();
    }

    public int getDuration() {
        return duration;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getGenre() {
        return genre;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Composition{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", duration=" + duration +
                ", genre='" + genre + '\'' +
                '}';
    }
}
