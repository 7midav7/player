package com.marchenko.player.entity.composition;


/**
 * Created by Marchenko Vadim on 9/22/2015.
 */
public class Electronic extends Composition {
    private int beatsPerSecond;

    public Electronic(int id, int duration, String name, int beatsPerSecond){
        super(id, duration, name, "electronic");
        setBeatsPerSecond(beatsPerSecond);
    }

    public int getBeatsPerSecond() {
        return beatsPerSecond;
    }

    public void setBeatsPerSecond(int beatsPerSecond){
        this.beatsPerSecond = beatsPerSecond;
    }

}
