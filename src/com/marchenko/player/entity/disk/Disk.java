package com.marchenko.player.entity.disk;

import com.marchenko.player.entity.composition.Composition;

import java.util.ArrayList;

/**
 * Created by Marchenko Vadim on 9/23/2015.
 */
public class Disk extends ArrayList<Composition>{
    private int id;
    private String name;
    private TypeDisk type;

    public Disk(int id, String name, TypeDisk type) {
        this.id = id;
        this.name = name;
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public TypeDisk getType() {
        return type;
    }

    @Override
    public String toString() {
        return "Disk{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", type=" + type +
                '}';
    }
}
