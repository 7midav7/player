package com.marchenko.player.entity.disk;

/**
 * Created by Marchenko Vadim on 9/25/2015.
 */
public enum TypeDisk {
    DVD1(1, 1, 8, 1.46), DVD2(1, 2, 8, 2.66), DVD3(2, 2, 8, 2.92),
    DVD4(2, 4, 8, 5.32), DVD5(1, 1, 12, 4.70), DVD9(1, 2, 12, 8.54),
    DVD10(2, 2, 12, 9.4), DVD14(2, 3, 12, 13.24), DVD18(2, 4, 12, 17.08);

    private int sides;
    private int layers;
    private int diameter;
    private double capacityGB;

    TypeDisk(int sides, int layers, int diameter, double capacityGB) {
        this.sides = sides;
        this.layers = layers;
        this.diameter = diameter;
        this.capacityGB = capacityGB;
    }

    public int getSides() {
        return sides;
    }

    public int getLayers() {
        return layers;
    }

    public int getDiameter() {
        return diameter;
    }

    public double getCapacityGB() {
        return capacityGB;
    }
}
