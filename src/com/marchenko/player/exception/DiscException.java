package com.marchenko.player.exception;

/**
 * Created by Marchenko Vadim on 9/24/2015.
 */
public class DiscException extends Exception {
    public DiscException() {
    }

    public DiscException(Throwable cause) {
        super(cause);
    }

    public DiscException(String message) {
        super(message);
    }

    public DiscException(String message, Throwable cause) {
        super(message, cause);
    }

    public DiscException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
