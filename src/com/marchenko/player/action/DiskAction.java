package com.marchenko.player.action;

import com.marchenko.player.entity.composition.Composition;
import com.marchenko.player.entity.disk.Disk;

import java.util.ArrayList;
import java.util.Collections;
import java.util.stream.Collectors;

/**
 * Created by Marchenko Vadim on 9/24/2015.
 */
public class DiskAction {
    public void recordDisk(Disk disk, ArrayList<Composition> compositions){
        disk.addAll(compositions);
    }

    public ArrayList<Composition> search(Disk disk, int minRange, int maxRange){
        return disk.stream()
                .filter(comp -> ( comp.getDuration() >= minRange && comp.getDuration() <= maxRange ))
                .collect(Collectors.toCollection(ArrayList::new));
    }

    public void sortGenre(Disk disk){
        Collections.sort(disk, (comp1, comp2) -> (comp1.getGenre().compareTo(comp2.getGenre())));
    }
}
