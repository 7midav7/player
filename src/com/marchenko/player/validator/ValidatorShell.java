package com.marchenko.player.validator;

/**
 * Created by Marchenko Vadim on 9/25/2015.
 */
public class ValidatorShell<T> {
    private T uncheckedObject;
    private Validator<T> validator;

    public ValidatorShell(T uncheckedObject, Validator<T> validator) {
        this.uncheckedObject = uncheckedObject;
        this.validator = validator;
    }

    public T getUncheckedObject() {
        return uncheckedObject;
    }

    public void setUncheckedObject(T uncheckedObject) {
        this.uncheckedObject = uncheckedObject;
    }

    public Validator<T> getValidator() {
        return validator;
    }

    public void setValidator(Validator<T> validator) {
        this.validator = validator;
    }

    public boolean isValid(){
        return validator.isValid(uncheckedObject);
    }
}
