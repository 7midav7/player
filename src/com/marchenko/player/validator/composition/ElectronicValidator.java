package com.marchenko.player.validator.composition;

import com.marchenko.player.entity.composition.Electronic;

/**
 * Created by Marchenko Vadim on 9/23/2015.
 */
public class ElectronicValidator extends CompositionValidator{

    public static final int MIN_RANGE_BEATS_PER_SECOND = 30;
    public static final int MAX_RANGE_BEATS_PER_SECOND = 240;

    public boolean isValidBeatsPerSecond(int bits){
        return MIN_RANGE_BEATS_PER_SECOND <= bits && bits <= MAX_RANGE_BEATS_PER_SECOND;
    }

    public boolean isValid(Electronic obj) {
        return super.isValid(obj) &&
                isValidBeatsPerSecond(obj.getBeatsPerSecond());
    }
}
