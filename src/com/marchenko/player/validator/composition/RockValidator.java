package com.marchenko.player.validator.composition;

import com.marchenko.player.entity.composition.Rock;

/**
 * Created by Marchenko Vadim on 9/24/2015.
 */
public class RockValidator extends CompositionValidator{
    public boolean isValidVocal(String vocal){
        return vocal!=null && !vocal.isEmpty();
    }

    public boolean isValidBand(String band){
        return band!=null && !band.isEmpty();
    }

    public boolean isValid(Rock rock){
        return super.isValid(rock) && isValidBand(rock.getBand()) && isValidVocal(rock.getVocal());
    }
}
