package com.marchenko.player.validator.composition;

import com.marchenko.player.entity.composition.Classical;

/**
 * Created by Marchenko Vadim on 9/25/2015.
 */
public class ClassicalValidator extends CompositionValidator {
    public boolean isValidOrchestraName(String name){
        return name != null && !name.isEmpty();
    }

    public boolean isValid(Classical classical){
        return super.isValid(classical) && isValidOrchestraName(classical.getName());
    }
}
