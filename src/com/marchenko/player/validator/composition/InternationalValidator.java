package com.marchenko.player.validator.composition;

import com.marchenko.player.entity.composition.International;

/**
 * Created by Marchenko Vadim on 9/24/2015.
 */
public class InternationalValidator extends CompositionValidator {
    public boolean isValidRegion(String region){
        return region != null && !region.isEmpty();
    }

    public boolean isValid(International international){
        return super.isValid(international) && isValidRegion(international.getRegion());
    }
}
