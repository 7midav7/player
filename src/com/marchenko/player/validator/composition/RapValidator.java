package com.marchenko.player.validator.composition;

import com.marchenko.player.entity.composition.Rap;

/**
 * Created by Marchenko Vadim on 9/24/2015.
 */
public class RapValidator extends CompositionValidator {
    public boolean isValidRapper(String rapper){
        return rapper!=null && !rapper.isEmpty();
    }

    public boolean isValid(Rap rap){
        return super.isValid(rap) && isValidRapper(rap.getRapper());
    }
}
