package com.marchenko.player.validator.composition;

import com.marchenko.player.entity.composition.Composition;

/**
 * Created by Marchenko Vadim on 9/23/2015.
 */
public class CompositionValidator{
    public boolean isValidDuration(int duration){
        return duration > 0;
    }

    public boolean isValidId(int id){
        return id > 0;
    }

    public boolean isValidName(String name){
        return name != null && !name.isEmpty();
    }

    public boolean isValid(Composition composition){
        boolean result;

        result = isValidDuration(composition.getDuration());
        result &= isValidId(composition.getId());
        result &= isValidName(composition.getName());
        result &= isValidGenre(composition.getGenre());

        return result;
    }

    public boolean isValidGenre(String name){
        return name != null && !name.isEmpty();
    }
}
