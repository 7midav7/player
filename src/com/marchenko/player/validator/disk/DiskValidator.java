package com.marchenko.player.validator.disk;

import com.marchenko.player.entity.disk.Disk;

/**
 * Created by Marchenko Vadim on 9/23/2015.
 */
public class DiskValidator {

    public boolean isValidId(int id){
        return id > 0;
    }

    public boolean isValidName(String name){
        return name!= null && !name.isEmpty();
    }

    public boolean isValid(Disk disk){
        return isValidId(disk.getId()) && isValidName(disk.getName());
    }

}
