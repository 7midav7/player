package com.marchenko.player.validator;

/**
 * Created by Marchenko Vadim on 9/25/2015.
 */
public interface Validator<T> {
    boolean isValid(T uncheckedObject);
}
