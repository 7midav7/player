package com.marchenko.player.demonstrator;

import com.marchenko.player.action.DiskAction;
import com.marchenko.player.creator.CompositionCreator;
import com.marchenko.player.creator.DiskCreator;
import com.marchenko.player.entity.composition.Composition;
import com.marchenko.player.entity.disk.Disk;
import com.marchenko.player.exception.DiscException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;

/**
 * Created by Marchenko Vadim on 9/24/2015.
 */
public class Demonstrator {
    private static final Logger LOGGER = LogManager.getLogger(Demonstrator.class);
    private static final int MIN_DURATION_RANGE = 0;
    private static final int MAX_DURATION_RANGE = 40;

    public void demonstrate() throws DiscException{
        CompositionCreator creator = new CompositionCreator();
        DiskCreator diskCreator = new DiskCreator();

        ArrayList<Composition> compositions = creator.create();
        Disk disk = diskCreator.create();

        recordComposition(disk, compositions);
        searchComposition(disk);
        sortGenre(disk);
    }

    private void recordComposition(Disk disk, ArrayList<Composition> compositions){
        LOGGER.debug("Start recording...");

        DiskAction da = new DiskAction();
        da.recordDisk(disk, compositions);

        LOGGER.debug(disk);
        LOGGER.debug("Finish recording.");
    }

    private void searchComposition(Disk disk){
        LOGGER.debug("Start searching...");

        DiskAction da = new DiskAction();
        ArrayList<Composition> compositions = da.search(disk, MIN_DURATION_RANGE, MAX_DURATION_RANGE);

        LOGGER.debug(compositions);
        LOGGER.debug("Finish searching.");
    }

    private void sortGenre(Disk disk){
        LOGGER.debug("Start sorting ...");

        DiskAction da = new DiskAction();
        da.sortGenre(disk);

        ArrayList<Composition> compositions = new ArrayList<>(disk);
        LOGGER.debug(compositions);
        LOGGER.debug("Finish sorting.");
    }
}
