package com.marchenko.player.main;


import com.marchenko.player.demonstrator.Demonstrator;
import com.marchenko.player.exception.DiscException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


/**
 * Created by Marchenko Vadim on 9/22/2015.
 */
public class Main {
    private static final Logger LOGGER = LogManager.getLogger(Main.class);
    private static final String LOGS_CONFIG_URI = "File:\\config\\log4j2.xml";

    static{
        System.setProperty("log4j.configurationFile", LOGS_CONFIG_URI);
    }

    public static void main(String[] args) throws DiscException{
        Demonstrator demonstrator = new Demonstrator();
        demonstrator.demonstrate();
    }
}
